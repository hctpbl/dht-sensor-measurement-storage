import logging
from time import sleep

import Adafruit_DHT
from influxdb import InfluxDBClient

import settings

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

if __name__ == '__main__':

    influx_db_settings = settings.INFLUX_SETTINGS
    client = InfluxDBClient(
        influx_db_settings['HOST'],
        influx_db_settings['PORT'],
        influx_db_settings['USER'],
        influx_db_settings['PASSWORD'],
        influx_db_settings['DATABASE']
    )

    while True:
        humidity, temperature = Adafruit_DHT.read_retry(settings.DHT_SENSOR, settings.SENSOR_OUT_PIN)

        if humidity is not None and temperature is not None:
            logger.info('Temp={0:0.1f}Cº  Humidity={1:0.1f}%'.format(temperature, humidity))
            json_body = [
                {
                    "measurement": "environment",
                    "tags": {
                        "room": "living_room"
                    },
                    "fields": {
                        "temperature": temperature,
                        "humidity": humidity
                    }
                }
            ]
            try:
                client.write_points(json_body)
            except Exception as e:
                logger.error("Unable to write temperature measure to InfluxDB. Data point lost.")
        else:
            logger.warning('Failed to get reading. Trying again after poll time.')

        sleep(settings.SECONDS_BETWEEN_READINGS)
