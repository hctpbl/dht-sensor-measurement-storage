import Adafruit_DHT

DHT_SENSOR = Adafruit_DHT.DHT22
SENSOR_OUT_PIN = 2
SECONDS_BETWEEN_READINGS = 20

INFLUX_SETTINGS = {
    "HOST": "localhost",
    "PORT": 8086,
    "USER": "",
    "PASSWORD": "",
    "DATABASE": "home",
}